// hugo-slugify-string.go
//
// Originally from https://github.com/gohugoio/hugo/blob/master/helpers/path.go.
// Copied UnicodeSanitize() and ishex() to convert file name to Hugo's slug in command line.
// The method of UnicodeSanitize() is modified to control lower case conversion and remove unicode accents. However, currently, always file name is converted to lower case and no unicode accents remove is done.
package main

import "os"
import "fmt"
import "unicode"
import "strings"
import "golang.org/x/text/runes"
import "golang.org/x/text/transform"
import "golang.org/x/text/unicode/norm"

// From https://golang.org/src/net/url/url.go
func ishex(c rune) bool {
  switch {
    case '0' <= c && c <= '9':
      return true
    case 'a' <= c && c <= 'f':
      return true
    case 'A' <= c && c <= 'F':
      return true
  }
  return false
}

func UnicodeSanitize(s string, removePathAccents bool, forceLowerCase bool) string {
  source := []rune(s)
  target := make([]rune, 0, len(source))
  var prependHyphen bool

  for i, r := range source {
    isAllowed := r == '.' || r == '/' || r == '\\' || r == '_' || r == '#' || r == '+' || r == '~'
    isAllowed = isAllowed || unicode.IsLetter(r) || unicode.IsDigit(r) || unicode.IsMark(r)
    isAllowed = isAllowed || (r == '%' && i+2 < len(source) && ishex(source[i+1]) && ishex(source[i+2]))

    if isAllowed {
      if prependHyphen {
        target = append(target, '-')
        prependHyphen = false
      }
      target = append(target, r)
    } else if len(target) > 0 && (r == '-' || unicode.IsSpace(r)) {
      prependHyphen = true
    }
  }

  var result string
  if removePathAccents {
    // remove accents - see https://blog.golang.org/normalization
    t := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
    result, _, _ = transform.String(t, string(target))
  } else {
    result = string(target)
  }

  if forceLowerCase {
    result = strings.ToLower(result)
  }
  return result
}

func main() {
  argCount := len(os.Args[1:])
  if (argCount < 1) {
    fmt.Println("'this is a file name'")
    os.Exit(1)
  }

  fmt.Println(UnicodeSanitize(os.Args[1], false, true))
  os.Exit(0)
}
